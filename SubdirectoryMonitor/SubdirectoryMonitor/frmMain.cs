﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using SubdirectoryMonitor.Properties;

/*
 * A quick hack. This creates a small display that shows how many files exist
 * in the various subdirectories of a selected directory, but NOT in the
 * subdirectories of those subdirectories.
 * 
 * Why would you want that? Well, in my case, I have a bunch of folders
 * where files are placed to be printed, and then under each one, a
 * Printed folder where they're moved after printing. I want to know how
 * many files are waiting for printing at any given moment, but not to
 * count all the ones already printed.
 * 
 * This program is a steamroller to kill a fly, but I only have the need
 * temporarily while testing something.
 * 
 * Frank J. Perricone, hawthorn@foobox.com
 * 
 */

namespace SubdirectoryMonitor
{
  public partial class frmSubdirectoryMonitor : Form
  {

    string FullDirectoryName = "";

    // centralized place to set the directory, so the command line parameters can too
    public void SetDirectory(string theDirectory) {
      if (Directory.Exists(theDirectory))
      {
        FullDirectoryName = theDirectory;

        // display only the folder name itself
        lblDirectory.Text = Path.GetFileName(theDirectory);

        // enable and fire the timer to start showing counts every two seconds
        timerTickTock.Enabled = true;
        timerTickTock_Tick(null, null);
      }
    }

    public frmSubdirectoryMonitor()
    {
      InitializeComponent();
    }

    /*
     * Let the user choose a directory to monitor.
     * Display just the directory name, not the full path,
     * but save the full path too.
     */
    private void bntChooseDirectory_Click(object sender, EventArgs e)
    {
      // select a directory
      DialogResult folderResult = folderBrowserDialog1.ShowDialog();
      if (folderResult != DialogResult.OK) return;

      SetDirectory(folderBrowserDialog1.SelectedPath);      
    }

    /*
     * The timer that updates and displays the count every two seconds
     */
    private void timerTickTock_Tick(object sender, EventArgs e)
    {
      // if lblDirectory has a valid directory
      if (Directory.Exists(FullDirectoryName))
      {
        // count the number of files in subdirs
        int FileCount = 0;
        string[] Subdirs;
        try
        {
          Subdirs = Directory.GetDirectories(FullDirectoryName);
        }
        catch
        {
          // if there are no subdirectories the answer is zero
          lblFileCount.Text = "0";
          return;
        }
        foreach (string Dirname in Subdirs) {
          string[] FilesInSubdir;
          try
          {
            FilesInSubdir = Directory.GetFiles(Dirname);
          }
          catch
          {
            continue;
          }
          FileCount += FilesInSubdir.Count();
        }

        lblFileCount.Text = FileCount.ToString();
      }
      else
      {
        // if it's not a directory, just put an error message
        lblFileCount.Text = "?";
      }
    }

    /*
     * Whenever changing the displayed count, change the font size
     * so that the display fits the space pretty well
     */
    private void lblFileCount_TextChanged(object sender, EventArgs e)
    {
      int numChars = lblFileCount.Text.Length;
      int newFontSize = 7 - numChars;
      if (newFontSize < 1) newFontSize = 1;
      lblFileCount.Font = new Font(lblFileCount.Font.Name, newFontSize * 12,
                lblFileCount.Font.Style, lblFileCount.Font.Unit);
    }

    /*
     * When closing the form, save its location so it can be opened there next time
     */
    private void frmSubdirectoryMonitor_FormClosing(object sender, FormClosingEventArgs e)
    {
      Settings.Default.WindowLocation = this.Location;
      Settings.Default.Save();
    }

    /*
     * When opening the form put it in the same spot it was last time
     */
    private void frmSubdirectoryMonitor_Load(object sender, EventArgs e)
    {
      if (Settings.Default.WindowLocation != null)
      {
        this.Location = Settings.Default.WindowLocation;
      }
    }
  }
}
