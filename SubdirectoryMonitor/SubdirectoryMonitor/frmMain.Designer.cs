﻿namespace SubdirectoryMonitor
{
  partial class frmSubdirectoryMonitor
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSubdirectoryMonitor));
      this.lblDirectory = new System.Windows.Forms.Label();
      this.bntChooseDirectory = new System.Windows.Forms.Button();
      this.lblFileCount = new System.Windows.Forms.Label();
      this.timerTickTock = new System.Windows.Forms.Timer(this.components);
      this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
      this.SuspendLayout();
      // 
      // lblDirectory
      // 
      this.lblDirectory.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblDirectory.Location = new System.Drawing.Point(6, 9);
      this.lblDirectory.Name = "lblDirectory";
      this.lblDirectory.Size = new System.Drawing.Size(152, 23);
      this.lblDirectory.TabIndex = 1;
      this.lblDirectory.Text = "(choose a directory)";
      this.lblDirectory.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      // 
      // bntChooseDirectory
      // 
      this.bntChooseDirectory.Location = new System.Drawing.Point(9, 25);
      this.bntChooseDirectory.Name = "bntChooseDirectory";
      this.bntChooseDirectory.Size = new System.Drawing.Size(149, 23);
      this.bntChooseDirectory.TabIndex = 2;
      this.bntChooseDirectory.Text = "Choose Directory";
      this.bntChooseDirectory.UseVisualStyleBackColor = true;
      this.bntChooseDirectory.Click += new System.EventHandler(this.bntChooseDirectory_Click);
      // 
      // lblFileCount
      // 
      this.lblFileCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblFileCount.Location = new System.Drawing.Point(12, 51);
      this.lblFileCount.Name = "lblFileCount";
      this.lblFileCount.Size = new System.Drawing.Size(146, 105);
      this.lblFileCount.TabIndex = 4;
      this.lblFileCount.Text = "?";
      this.lblFileCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.lblFileCount.TextChanged += new System.EventHandler(this.lblFileCount_TextChanged);
      // 
      // timerTickTock
      // 
      this.timerTickTock.Interval = 2000;
      this.timerTickTock.Tick += new System.EventHandler(this.timerTickTock_Tick);
      // 
      // frmSubdirectoryMonitor
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(170, 157);
      this.Controls.Add(this.lblFileCount);
      this.Controls.Add(this.bntChooseDirectory);
      this.Controls.Add(this.lblDirectory);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "frmSubdirectoryMonitor";
      this.Text = "Subdir Monitor";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmSubdirectoryMonitor_FormClosing);
      this.Load += new System.EventHandler(this.frmSubdirectoryMonitor_Load);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label lblDirectory;
    private System.Windows.Forms.Button bntChooseDirectory;
    private System.Windows.Forms.Label lblFileCount;
    private System.Windows.Forms.Timer timerTickTock;
    private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;

  }
}

