﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SubdirectoryMonitor
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main(string[] args)
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      frmSubdirectoryMonitor app = new frmSubdirectoryMonitor();

      // if there's a command line parameter, interpret the first one as a path to monitor
      if (args.Length >= 1)
      {
        app.SetDirectory(args[0]);
        // if you provide a bogus one, this will do nothing, so no problem
      }
      Application.Run(app);
      //Application.Run(new frmSubdirectoryMonitor());           
    }
  }
}
