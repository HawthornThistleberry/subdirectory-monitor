## What's this? ##

This program is a quick hack written in C# for Visual Studio 2012 targeting .NET Framework v4.0. It lets you choose a directory (or you can specify one on the command line), then continually (every two seconds) shows a count of how many files exist in the subdirectories of that directory, but not the sub-subdirectories. A pretty specific requirement, right? Well, that's what I needed to test a particular program, and it was a quick hack.

## Why would you need that? ##

Okay, if you really must know. I have a program which creates PDF files to be printed. There's a parent directory that has about 20 subdirectories, one for each printer, and the PDFs are dropped into the directory for the printer it should go to. Another program named Batch & Print Pro (third party, from Traction Software) notices the files there, prints them to the corresponding printer, then moves them to a Printed subdirectory under where the original file went.

While testing a problem with Batch & Print Pro I found myself having to check each of those about 20 directories over and over to see if any files were sitting there going unprinted. But I couldn't just check the whole parent directory, because everything that was printed, or had been printed in the last month, was also in there, in a sub-subdirectory. The Windows dir command can't readily do a recursive search of directories to a specified depth. I could probably hack something in PowerShell, but this seemed easier, and the result is cleaner to read -- one big number I can easily monitor out of the corner of my eye while watching other things. And it only took an hour or so to cobble together.

![SubdirInWin7.PNG](https://bitbucket.org/repo/69peaL/images/1486501343-SubdirInWin7.PNG) ![SubdirMonitor.png](https://bitbucket.org/repo/69peaL/images/2611941398-SubdirMonitor.png)